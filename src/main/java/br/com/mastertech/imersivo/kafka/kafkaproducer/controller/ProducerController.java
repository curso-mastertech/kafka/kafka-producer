package br.com.mastertech.imersivo.kafka.kafkaproducer.controller;

import br.com.mastertech.imersivo.kafka.kafkaproducer.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    @Autowired
    private KafkaProducerService producerService;

    @GetMapping("acesso/{cliente}/{porta}")
    private void registraAcesso(@PathVariable(value = "cliente") String cliente, @PathVariable(value = "porta") String porta) {
        producerService.registraAcesso(cliente, porta);
    }

}
