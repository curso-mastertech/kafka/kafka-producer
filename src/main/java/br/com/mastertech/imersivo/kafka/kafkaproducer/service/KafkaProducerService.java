package br.com.mastertech.imersivo.kafka.kafkaproducer.service;

import br.com.mastertech.imersivo.kafka.model.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class KafkaProducerService {

    @Autowired
    private KafkaTemplate<String, Acesso> sender;

    public void registraAcesso(String cliente, String porta) {
        boolean liberado = new Random().nextBoolean();
        sender.send("pedro", "1", new Acesso(cliente, porta, liberado));
    }
}
